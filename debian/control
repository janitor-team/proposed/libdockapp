Source: libdockapp
Section: libs
Priority: optional
Maintainer: Debian Window Maker Team <team+wmaker@tracker.debian.org>
Uploaders: Doug Torrance <dtorrance@piedmont.edu>,
           Andreas Metzler <ametzler@debian.org>,
           Jeremy Sowden <jeremy@azazel.net>
Build-Depends: debhelper-compat (= 12),
               libx11-dev,
               libxext-dev,
               libxpm-dev,
               pkg-config,
               xfonts-utils
Rules-Requires-Root: no
Standards-Version: 4.5.0
Homepage: https://www.dockapps.net/libdockapp
Vcs-Browser: https://salsa.debian.org/wmaker-team/libdockapp
Vcs-Git: https://salsa.debian.org/wmaker-team/libdockapp.git

Package: libdockapp3
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Suggests: libdockapp-dev, xfonts-libdockapp
Breaks: wmaker (<< 0.62.1)
Multi-Arch: same
Description: Window Maker Dock App support (shared library)
 Simple library that eases the creation of Window Maker dock apps. It
 provides functions that set up a dock app in such a way that they can be
 properly docked. It also provides some event handlers and makes it very
 simple to write dockapps.
 .
 This package contains the shared library. Install this for programs that
 use libdockapp.

Package: libdockapp-dev
Architecture: any
Section: libdevel
Depends: libdockapp3 (= ${binary:Version}),
         libx11-dev,
         libxpm-dev,
         ${misc:Depends}
Suggests: xfonts-libdockapp
Multi-Arch: same
Description: Window Maker Dock App support (development files)
 Simple library that eases the creation of Window Maker dock apps. It
 provides functions that set up a dock app in such a way that they can be
 properly docked. It also provides some event handlers and makes it very
 simple to write dockapps.
 .
 This package contains the static library and the required headers. Use it
 to compile programs which use libdockapp.

Package: xfonts-libdockapp
Architecture: all
Section: fonts
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Window Maker Dock App support (fonts)
 Simple library that eases the creation of Window Maker dock apps. It
 provides functions that set up a dock app in such a way that they can be
 properly docked. It also provides some event handlers and makes it very
 simple to write dockapps.
 .
 This package contains the X11 fonts which are included with libdockapp.
